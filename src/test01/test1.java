package src.test01;

public class test1 {
    private String name;
    private int age;

    //构造方法
    public test1(){
        System.out.println("无参构造方法");
    }

    public test1(String name,int age){
        this.name = name;
        this.age = age;
    }

    //成员方法
    public void setName(){
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setAge(){
        this.age=age;
    }

    public int getAge(){
        return age;
    }

    public void show(){
        System.out.println(name+age);
    }

}
